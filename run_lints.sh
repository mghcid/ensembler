#!/bin/bash -e


#ignored-modules=cosmos since pylint keep compalining about not being able to import cosmos
pylint ensembler/

# Disable F401: unused imports. Only really applies to __init__.py files, since all other unused imports
# Disable W391:  blank line at end of file but pylint requires a blank line at EOF!

flake8 --max-line-length=120 --ignore=F401 --ignore=W391 ensembler/