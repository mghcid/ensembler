"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import logging

logger = logging.getLogger(__name__)


def generate_key(record):
    """
    Given a vcf record this method returns a key based on chrom, pos, ref, alt
    :param record: pyvcf record object
    :return: string
    """
    if len(record.ALT) > 1:
        msg = 'Expected single alternative allele but found multiple alleles {}'.format(record.ALT)
        raise ValueError(msg)
    return '{0}_{1}_{2}_{3}'.format(record.CHROM, record.POS, record.REF, record.ALT[0])
