"""
(c) MGH Center for Integrated Diagnostics
"""

from __future__ import print_function
from __future__ import absolute_import


from setuptools import setup, find_packages
exec(open('ensembler/version.py').read())


def load_requirements():
    req_list = []
    with open('requirements.txt', 'r') as fileobj:
        for line in fileobj:
            if line.startswith('-e '):
                continue
            req_list.append(line.strip())
    return req_list

setup(
    name='ensembler',
    version=__version__,
    py_modules=['ensembler'],
    packages=find_packages(),
    package_data={'ensembler.templates': ['*.vcf'],
                  'ensembler.tests.data.vcf': ['*.vcf.gz']},
    include_package_data=True,
    url='https://bitbucket.org/mghcid/ensembler',
    author='Saeed Al Turki',
    author_email='salturki@gmail.com',
    license='MIT',
    install_requires=load_requirements(),
    entry_points='''
        [console_scripts]
        ensembler=ensembler.main:cli
    ''',
)
