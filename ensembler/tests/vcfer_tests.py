"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import os
import logging
import pkg_resources
# from nose.tools import assert_equal
# from ensembler.templates.vcfer import Vcf

logger = logging.getLogger(__name__)


class Test_Vcfer(object):
    """Unit testing for Vcfer class in ensembler.template"""

    test_files_path = pkg_resources.resource_filename('ensembler.tests', 'data')
    base_path = os.path.join(test_files_path, 'vcf')

    def test_rename_info(self):
        """Unit testing for rename_info_tags method"""
        path = os.path.join(self.base_path, 'lofreq.vcf.gz')
        print(path)
        # vcf_file = Vcf(path=path, caller='lofreq')
        # for record in vcf_file.reader:
        #     info = vcf_file.rename_info_tags(record.INFO)
        #     print(record)
        #     for key in info:
        #         print(key)
        #         assert_equal(key.startswith('lofreq'), True)
        #     break
