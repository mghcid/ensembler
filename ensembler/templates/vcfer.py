"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import logging

import vcf

logger = logging.getLogger(__name__)


class Vcf(object):
    """Vcf class"""
    def __init__(self, **kwargs):   # pylint: disable=E1101
        """
        Initiate vcf class which is the basis of all callers' vcf files e.g. GATK, LoFreq, etc. Create reader variable
        to vcf.Reader object
        :return: None
        """
        logger.debug('init %s lego', self.__class__.__name__)
        self.path = kwargs.get('path', None)
        self.name = kwargs.get('caller', None)
        self.reader = vcf.Reader(open(self.path, 'r'))
        self.update_header()

    def get_caller_fields(self):
        """
        Return specific tags from the INFO column
        :return: None
        """
        raise NotImplementedError()

    def update_header(self):
        """
        Update the header items in-place by adding the caller name as a prefix to the ID field in INFO, FILTER and
        FORMAT.
        :return: None
        """
        header_items = [self.reader.filters, self.reader.infos, self.reader.formats]
        for items in header_items:
            for old_key in items:
                if old_key.startswith(self.name):  # already processed. Exit the for loop
                    break
                new_key = '{0}_{1}'.format(self.name, old_key)
                items[old_key] = items[old_key]._replace(id=new_key)
                items[new_key] = items.pop(old_key)

    @property
    def full_path(self):
        """The full path to VCF file"""
        return self.path
