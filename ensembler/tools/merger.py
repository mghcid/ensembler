"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

from copy import deepcopy
from collections import OrderedDict, namedtuple
import logging
import click
import vcf

from ensembler.templates.vcfer import Vcf

logger = logging.getLogger(__name__)


def merge_header(master_path, master_caller, vcfs, callers, sample_id):
    """
    Create a master VCF header that include header lines from all callers after adding the caller name to each tag id.
    The final master vcf object can be used to with vcf.Writer for output. This method also add new header lines from
    for the CALLER detection status for a given variant. Finally change the fileformat to VCFv4.2
    :param master_path: Path to VCF file that will be used as template for master VCF
    :param master_caller: String caller name for the master VCF file
    :param vcfs: vcf read objects (i.e. based on vcf.reader from pyvcf package).
    :param callers: List of caller names
    :return: vcf read object with all tags from infos, formats and filters items in the header.
    """
    master_vcf = Vcf(path=master_path, caller=master_caller).reader  # will pick up the first one and add to it
    master_vcf.metadata['fileformat'] = 'VCFv4.2'  # Don't change this to 4.2  # pylint:disable=unsubscriptable-object
    info = namedtuple('Info', ['id', 'num', 'type', 'desc'])  # same as pyvcf's namedtuple used for info column

    # merge all vcf info, format and filter entries into the master vcf
    for vcf_file in vcfs:
        master_vcf.infos.update(vcf_file.infos)
        master_vcf.formats.update(vcf_file.formats)
        master_vcf.filters.update(vcf_file.filters)
        master_vcf.contigs.update(vcf_file.contigs)

    # add GT filter as a place holder since pyvcf need to see this field. The value will be ./.
    master_vcf.formats.update({'GT': info(id='GT', num=1, type='String', desc='Genotype place holder ./.')})

    old_basic = {}
    for caller in callers:
        for col_name in ['ID', 'QUAL', 'FILTER']:
            tag = "{0}_{1}".format(caller, col_name)
            old_basic[tag] = info(id=tag, num=1, type='String',
                                  desc='Original {0} column for {1} caller'.format(col_name, caller))
    master_vcf.infos.update(old_basic)

    # update header with callers lines
    if callers:
        callers_header = {}
        for caller in callers:
            caller_key = '{}_DETECT'.format(caller)
            callers_header[caller_key] = info(id=caller_key, num=1, type='String',
                                              desc='Boolean (Y or N) if {} has detected a variant'.format(caller))
        master_vcf.infos.update(callers_header)

    # add FORMAT and SAMPLE_ID to the #CHROM line
    if master_vcf.formats != {}:
        if len(master_vcf._column_headers) == 8:  # pylint: disable=W0212
            master_vcf._column_headers.extend(['FORMAT', sample_id.upper()])  # pylint: disable=W0212

    # include only one sample - mutect1 adds 'none' sample in random order
    if len(master_vcf.samples) > 1:
        master_vcf.samples = [sample for sample in master_vcf.samples if sample == sample_id]
    return master_vcf


def rename_tags(items, caller_name):
    """
    For every tag in the INFO or FORMAT column (dictionary of tag name and values), this method adds the caller name as
    a prefix to each key. For example for info tag DP called by LoFreq, the new key name is 'lofreq_DP'
    :param items: Dictionary
    :param caller_name: String caller name
    :return: Dictionary
    """
    holder = OrderedDict()
    if items:
        for key in items:
            new_key = '{0}_{1}'.format(caller_name, key)
            holder[new_key] = items[key]
    return holder or None


def merge_info(records, callers):
    """
    Merge variant's records from various VCF files (callers) into a single record. INFO columns and FORMAT columns
    will be merged (assuming their tags have been updated with the caller name as a prefix). ID, FILTER and QUAL will
    be added to the INFO Column and the original columns will be replaced with dot "."
    :param records: List of PyVCF Reader record objects
    :param callers: List of callers
    :return: Single PyVCF Reader record
    """
    holder = {}
    for dictionary in [rename_tags(record.INFO, callers[idx]) for idx, record in enumerate(records) if record]:
        if dictionary:
            holder.update(dictionary)
    return holder


def create_master_format(master_vcf):
    """
    When a record does not include FORMAT or SAMPLE columns this method create dummy or place holder values based on the
    FORMAT tag definitions in the VCF header.
    :param master_vcf: PyVCF's Reader objects to access the format tags in side the header
    :return: dummy_format tags as a string and dummy_sample with dots separated by :
    """
    dummy_format = OrderedDict()
    for tag in master_vcf.formats:
        dummy_format[tag] = '.'
    return dummy_format


def order_format_by_gt(callers, formats):
    """
    The first field in FORMAT column should bt GT. This method re-order by caller then make sure the first tag is GT.
    :param callers: List of callers
    :param formats: Dictionary of master record FORMAT key and values
    :return: OrderedDictionary
    """
    holder = OrderedDict()
    # add GT as  place holder to satisfy pyvcf need for GT field
    holder['GT'] = './.'
    for caller in callers:
        gt_tag = '{0}_GT'.format(caller)
        if gt_tag in formats:
            holder[gt_tag] = formats[gt_tag]
        for key in formats:
            if key.startswith(caller) and key not in holder:
                holder[key] = formats[key]
    return holder


def merge_format(caller_records, callers_names, master_record, master_format, reader_obj, sample_id):
    """
    Merge variant's FORMAT & SAMPLE columns from various VCF files (callers) into a single FORMAT & SAMPLE column after
    adding the caller_name as a prefix for each FORMAT tag.
    :param caller_records: List of PyVCF Reader record objects
    :param callers_names: List of caller names
    :param master_format:
    :param master_record: Record object from a PyVCF Reader
    :param reader_obj: The PyVCF Reader master object
    :param sample_id: String sample ID
    :return: Single PyVCF Reader's  record.samples.data object
    """

    for idx, record in enumerate(caller_records):
        if record is None:  # ignore if the variant is not detected by this caller
            continue
        if record.samples != []:
            sample = [call for call in record.samples if call.sample == sample_id][0]
            if sample:
                for j, old_format_tag in enumerate(sample.data._fields):
                    new_format_tag = '{0}_{1}'.format(callers_names[idx], old_format_tag)
                    master_format[new_format_tag] = sample.data[j] if sample.data[j] is not None else "."

    order_formats = order_format_by_gt(callers_names, master_format)
    format_tags = ":".join(order_formats.keys())
    format_values = ":".join([str(value) if not hasattr(value, '__iter__')
                              else ",".join([str(x) for x in value])
                              for value in order_formats.values()])
    # trick pyvcf to re-parse the FORMAT and SAMPLE as if this is the first time and return a proper data object
    master_record.FORMAT = format_tags
    master_record.samples = reader_obj._parse_samples([format_values], format_tags,
                                                      master_record)  # pylint:disable=protected-access
    return master_record


def dotify(value):
    """
    Given a vcf file, check if its value is empty and return dot, if it is a list return a string
    :param value: String or List
    :return: String
    """
    null_values = ['', None, []]
    if value in null_values:
        out_value = '.'
    elif hasattr(value, '__iter__'):
        out_value = value[0]
    else:
        out_value = value
    return out_value


def update_info_basic_columns(**kwargs):
    """
    Move the values of ID, FILTER and QUAL columns to the INFO where that tag has the caller name as a prefix
    :param kwargs: Dictionary
    :return: info object
    """
    master_record = kwargs.get('master_record')
    records = kwargs.get('records')
    callers = kwargs.get('callers')
    for idx, caller in enumerate(callers):
        caller_record = records[idx]
        if caller_record:
            master_record.INFO["{}_ID".format(caller)] = dotify(caller_record.ID)
            master_record.INFO["{}_FILTER".format(caller)] = dotify(caller_record.FILTER)
            master_record.INFO["{}_QUAL".format(caller)] = dotify(caller_record.QUAL)
    master_record.ID = '.'
    master_record.QUAL = '.'
    master_record.FILTER = '.'
    return master_record


def update_info_callers(master_record, records, callers):
    """
    Add caller tags to the INFO column in the master record to indicate if the caller has detected the variant or not.
    :param master_record:  PyVCF's Reader record object
    :param records:  PyVCF's Reader record objects
    :param callers: List of caller names
    :return: master_record after updating the info columns
    """
    for idx, caller in enumerate(callers):
        caller_tag = '{}_DETECT'.format(caller)
        master_record.INFO[caller_tag] = 'N' if records[idx] is None else 'Y'
    return master_record


def load_variants(callers, readers, sample_id):
    """
    Load variants records to a dictionary and where keys are (chrom, pos, ref, alt)
    :param callers: List of callers
    :param readers: PyVcf reader objects
    :param sample_id: String sample id
    :return: Dictionary of PyVcf reader records
    """
    variants = {}
    for idx, reader in enumerate(readers):
        for record in reader:
            items = [record.CHROM, record.POS, record.REF, record.ALT[0]]
            if record.is_sv:
                # use the CNV or SV's END to make sure we don't overwrite CNV with same start but different ENDs
                items.append(record.INFO['END'])
            key = "_".join([str(x) for x in items])

            if key not in variants:
                variants[key] = {caller: None for caller in callers}
            record.samples = [call for call in record.samples if call.sample == sample_id]
            variants[key][callers[idx]] = record
    return variants


def main(paths, output_vcf, sample_id):
    """
    Merge input files into a single VCF file. INFO tags will be prefixed by the caller name to differentiate where
     each tag comes from.
    :param paths: A dictionary to paths to callers VCF files
    :param output_vcf: A full path to write an output VCF file
    :param sample_id: String
    :return: Path to ensemble VCF
    """
    vcf_readers = []
    callers = []

    for item in paths:
        caller, path = item.split(',')
        callers.append(caller)
        vcf_file = Vcf(path=path, caller=caller)
        vcf_readers.append(vcf_file.reader)

    master_caller, master_path = paths[0].split(',')
    master_vcf = merge_header(master_path, master_caller, vcf_readers, callers, sample_id)
    master_format = create_master_format(master_vcf)
    o_vcf = vcf.Writer(open(output_vcf, 'w'), master_vcf)
    variants = load_variants(callers, vcf_readers, sample_id)
    for key in sorted(variants):
        records = [variants[key][caller] for caller in callers]
        master_record = deepcopy([record for record in records if record is not None][0])
        master_record.INFO = merge_info(records, callers)
        master_record = update_info_basic_columns(master_record=master_record, callers=callers, records=records)
        master_record = merge_format(records, callers, master_record, deepcopy(master_format),
                                     [reader for reader in vcf_readers if reader.samples != []][0], sample_id)
        master_record = update_info_callers(master_record, records, callers)
        o_vcf.write_record(master_record)


@click.group(invoke_without_command=True)
@click.option('--vcf', '-v', multiple=True, help='caller name and full path to vcf file separated by comma '
                                                 '(e.g. gatk,file.vcf.gz)')
@click.option('--output-vcf', '-o', help='Full path to save a vcf file')
@click.option('--sample-id', '-s', required=True, help='Sample id to add to the VCF header. No spaces allowed.')
def cli(**kwargs):
    """Merge different VCF different callers"""

    # check arguments
    for item in kwargs['vcf']:
        if ',' not in item:
            raise ValueError('Missing comma separated format for one --vcf arguments: caller,path "{}"'.format(item))

    if len(kwargs['vcf']) == 0:
        raise ValueError('Expected at least one vcf file (--vcf)"')

    if ' ' in kwargs['sample_id']:
        raise ValueError('Error: found white space in sample-id argument "{}"'.format(kwargs['sample_id']))

    main(kwargs['vcf'], kwargs['output_vcf'], kwargs['sample_id'])

if __name__ == "__main__":
    cli()
