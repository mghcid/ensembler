"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import logging
import pkg_resources

import click
import pysam
from pybedtools import BedTool

from htqc.bam.coverage import Coverage

logger = logging.getLogger(__name__)


class HotspotCaller(object):
    """ Hotspot caller calls"""
    def __init__(self, bed_path, bam_path, ref_path, minimum_reads, output_path):
        """
        Initiate the hotspot caller
        :param bed_path: Path to a list of variants in a BED format
        :param bam_path: BAM file path
        :param ref_path: Genome reference path
        :return: None
        """
        self.bed = BedTool(bed_path)
        self.bam = pysam.AlignmentFile(bam_path)  # pylint: disable=E1101
        self.genome = pysam.Fastafile(ref_path)   # pylint: disable=E1101
        self.minimum_reads = minimum_reads
        self.output_path = output_path
        self.coverage = Coverage(bam_path, bed_path=bed_path, count_bases=True)

    def get_reference_allele(self, chrom, start, end):
        """
        Extract the reference allele
        :param chrom: String
        :param start: Integer
        :param end: Integer
        :return: String
        """
        return self.genome.fetch(region="{0}:{1}-{2}".format(chrom, start, end - 1))

    def get_read_counts(self, chrom, start, end):
        """
        Return all alternative allele if the bam file doesn't match the ref_allele. Needs at least 2 reads supporting
        alternative allele.
        :param chrom: String
        :param start: Integer
        :param end: Integer
        :return: Dictionary of raw, hq+, hq- read counts for each nucleotide.
        (hq is high quality + plus strand and - is minus strand)
        """
        if start == end:
            end += 1
        return self.coverage.bases_counter(chrom, start, end)

    def get_header(self):
        """
        Get template VCF header
        :return: String
        """
        with open(pkg_resources.resource_filename('ensembler.templates', 'template.vcf')) as vcf_template:
            header = vcf_template.read().format(self.genome.filename)  # add the genome reference to the vcf header
        return header

    def get_alternative_alleles(self, read_counts, ref_allele):
        """
        For each nucleotide get the count of raw, hq+ and hq- if raw >= 2 reads
        :param read_counts: Dictionary from HTQC coverage
        :param ref_allele: String
        :return: Dictionary of alternative alleles and their coverage as INFO tag,value
        """
        holder = {}
        for alt in 'ACGT':
            if alt == ref_allele:
                continue  # this is reference, ignore
            if read_counts['hq+'][alt] + read_counts['hq-'][alt] >= self.minimum_reads:
                alt_hq_count = read_counts['hq+'][alt] + read_counts['hq-'][alt]
                ref_hq_count = read_counts['hq+'][ref_allele] + read_counts['hq-'][ref_allele]
                alt_hq_af = float(alt_hq_count) / (alt_hq_count + ref_hq_count)
                holder[alt] = "AF={0};DP={1},{2}".format(alt_hq_af, ref_hq_count, alt_hq_count)
        return holder

    def save_vcf(self):
        """
        Write all hotspot variants that has to a VCF file
        :return: None
        """
        with open(self.output_path, 'w') as vcf_writer:
            vcf_writer.write(self.get_header())
            for variant in self.bed:
                chrom, start, end = variant.chrom, variant.start, variant.end
                if start == end:
                    raise ValueError('The variant start equals end in BED file: {0}:{1}-{2}'.format(chrom, start, end))
                if start + 1 != end:
                    continue  # ignore indels. TODO deal with indels
                ref = self.get_reference_allele(chrom, start, end)
                if ref in [None, '']:
                    raise ValueError('Error: Does the chromosome has a prefix (Chr or chr) not matching the BAM?')
                read_counts = self.get_read_counts(chrom, start, end)
                alts = self.get_alternative_alleles(read_counts, ref)
                for alt in alts:
                    record = [str(x) for x in [variant.chrom, variant.start, '.', ref, alt, '.', 'PASS', alts[alt]]]
                    vcf_writer.write("\t".join(record) + "\n")


@click.group(invoke_without_command=True)
@click.option('--bed-path', '-b', type=click.Path(exists=True), help='Path to a BED file of variants (only SNV)')
@click.option('--bam-path', '-m', type=click.Path(exists=True), help='Path to a BAM file ')
@click.option('--ref-path', '-r', type=click.Path(exists=True), help='Path to a reference genome')
@click.option('--output-path', '-o', help='Path to save a vcf file')
@click.option('--min-reads', '-n', help='Minimum number of reads to call an alternative allele (default 5)', default=5)
def cli(**kwargs):
    """Forced calling hotspot variants (SNV only)"""
    hotspot = HotspotCaller(bed_path=kwargs['bed_path'], bam_path=kwargs['bam_path'],
                            ref_path=kwargs['ref_path'], output_path=kwargs['output_path'],
                            minimum_reads=kwargs['min_reads'])
    hotspot.save_vcf()


if __name__ == "__main__":
    cli()
