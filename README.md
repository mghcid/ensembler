## Overview ##

Ensembler caller includes the following 3 tools:

1. Merger: To merged unlimited number of VCF files and rename each tag with caller prefix.
2. Hotspotter: Force calling hotspot variants (SNVs) based on a BED file
3. Distiller: Based on analytics model (using synthetic data), annotate each variants with AUC [PENDING]
 
![Alt text](https://bitbucket.org/mghcid/ensembler/raw/master/docs/ensembler_overview.png)

## Installation ##

```console
git clone git@bitbucket.org:mghcid/ensembler.git
cd ensembler
pip install . 
```

## Usage ##

Currently there are two tools aviable as stand alone commands ```ensembler merger``` to merge vcf files and 
```ensembler hotspotter``` to force calling hotspot SNV variant from a bam file given a bed file. 

### Merger tool ###
To merge VCF files using the following command:

The ```merger``` sub-command accept few options which are listed by adding ```--help``` 

```console
ensembler merger --help

Usage: ensembler merger [OPTIONS] COMMAND [ARGS]...

  Merge different VCF different callers

Options:
  -v, --vcf TEXT         caller name and full path to vcf file separated by comma (e.g. gatk,file.vcf.gz)
  -b, --bam PATH         Path to a BAM file to add coverage counts of F+/R+ and F-/R- reads for each variant
  -o, --output-vcf TEXT  Full path to save a vcf file
  --help                 Show this message and exit.
```

* ```--vcf``` argument accepted unlimited number of vcf files. The accepted format is a pair of caller name and a path to vcf file as ```gatk,file.vcf.gz```. 
* ```--bam``` to get HTQC coverage for each variant, supply a path to bam file. 
* ```--outpu-vcf``` is the output vcf file. 

### List of operations performed by Merger tool ###

1. During merging process, some fields like DP in INFO column comes from different VCF files and will overwrite previous one. To avoid this; the ```merger``` tool renames the tag name by adding the caller name as prefix to the tag's name. For example, DP tag from MuTect and GATK renamed as ```mutect_DP``` and ```gatk_DP```, respectively. This is important to help us differentiate what is the source of DP value instead of overwriting it. This renaming is applied to all fields (ID, FILTER, QUAL, FORMAT, SAMPLE). This will enlarge the file size but it is a small price to keep all fields clear and defined. 

2. A new INFO tag is added to indicate of a given variant was detected by a caller or not. These tags are names as ```CALLER_DETECTED=Y | N```. For example, ```mutect_DETECTED=Y;lofreq_DETECTED=N``` which indicate a variant was missed by LoFreq but detected by MuTect. This is important for the Analytics model training later. 

3. If a bam file is given, the ```merger``` will add 3 header lines for HTQC coverage data. Here are the lines along with their descriptions:

```
##INFO=<ID=HTQC_HQ_COLPS,Number=1,Type=Integer,Description="As HTQC_FLT but paired-end reads collapsed)">
##INFO=<ID=HTQC_ABS,Number=1,Type=Integer,Description="Median nbr of reads without any filters">
##INFO=<ID=HTQC_HQ,Number=1,Type=Integer,Description="Median nbr of reads with MQ > 20">
```

```merger``` calculates the coverage for each  SNV and INDEL variants using HTQC library. CNV, SV and FUSIONS are ignored.

Finally, here is an example of full command line
```console
ensembler merger --vcf lofreq,lofreq_concatenated.vcf.gz --vcf mutect,mutect_concatenated.vcf.gz --vcf gatk,gatk_concatenated.vcf.gz --bam all_chroms.marked_dup.bam -o output.vcf 
```

### Hotspotter tool ###
  
Given a list of variant in a BED format , this tool will force call all alternative alleles that are supported by 2 or more raw reads (it doesn't check for mapping or base quality in order to maximize the sensitivity. The tool is designed for SNV only and ignores other variants. INDELs will be added later. It has 5 arguments:

```console
ensembler hotspotter --help

  -b, --bed-path PATH      Path to a BED file of variants (only SNV)
  -m, --bam-path PATH      Path to a BAM file
  -r, --ref-path PATH      Path to a reference genome
  -o, --output-path TEXT   Path to save a vcf file
  -n, --min-reads INTEGER  Minimum number of reads to call an alternative allele (default 2)
  --help                   Show this message and exit.
```

Example of a command line:

```console
ensembler hotspotter --bed-path hotspot_synth.bed --bam-path all_chroms.marked_dup.bam --ref-path Homo_sapiens_assembly19.fasta --output-path output.vcf
```

### Distiller tool ###

Under development.